package com.posco.mes3.n1b.material.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.posco.mes3.n1b.material.store.jpo.SlabJpo;

//두번째 인자 슬라브 데이타 타입
public interface SlabRepository extends JpaRepository<SlabJpo, String> {


}
