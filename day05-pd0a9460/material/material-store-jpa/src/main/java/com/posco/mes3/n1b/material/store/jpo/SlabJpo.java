package com.posco.mes3.n1b.material.store.jpo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.posco.mes3.n1b.material.entity.Dimension;
import com.posco.mes3.n1b.material.entity.Slab;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(schema="", name="TB_SLAB")
public class SlabJpo {
	//
	@Id
	private String id;

	
	private double thickness;
	
	@Column(name="DIMS", nullable = false)
	private String dimensionJson;
	
	
	
	public SlabJpo(Slab slab) {
		//
		this.id = slab.getId();
		this.thickness = slab.getDimension().getThickiness();
		this.dimensionJson = slab.getDimension().toJson();
	}
	
	public Slab toDomain() {
		//
		Slab slab = new Slab(this.id);
		slab.setDimension(Dimension.fromJson(dimensionJson));
		return slab;
	}
}












