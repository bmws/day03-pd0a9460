package com.posco.mes3.n1b.material.store;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.posco.mes3.n1b.material.entity.Slab;
import com.posco.mes3.n1b.material.store.jpo.SlabJpo;
import com.posco.mes3.n1b.material.store.repository.SlabRepository;

@Repository
public class SlabJapStore implements SlabStore{

	private final SlabRepository slabRepository;
	
	
	public SlabJapStore(SlabRepository slabRepository) {
		//super();
		this.slabRepository = slabRepository;
	}

	@Override
	public void create(Slab slab) {
		// TODO Auto-generated method stub
		this.slabRepository.save(new SlabJpo(slab));
	}

	@Override
	public Slab retrieve(String id) {
		// TODO Auto-generated method stub
		Optional<SlabJpo> ids = slabRepository.findById(id);
		
		//해당건이 있는지 확인
		if(!ids.isPresent()) {
			throw new NoSuchElementException(String.format("slab(%s) is not found", id));
		}
		
		return ids.get().toDomain();
	}

	@Override
	public void update(Slab slab) {
		// TODO Auto-generated method stub
		this.slabRepository.save(new SlabJpo(slab));
		
	}

	@Override
	public void delete(Slab slab) {
		// TODO Auto-generated method stub
		this.slabRepository.delete(new SlabJpo(slab));
		
	}

}
