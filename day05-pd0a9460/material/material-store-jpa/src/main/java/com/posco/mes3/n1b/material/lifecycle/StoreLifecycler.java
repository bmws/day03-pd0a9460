package com.posco.mes3.n1b.material.lifecycle;

import org.springframework.stereotype.Component;

import com.posco.mes3.n1b.material.store.SlabStore;

@Component
public class StoreLifecycler implements StoreLifecycle {

	private SlabStore slabStore;
	
	
	
	public StoreLifecycler(SlabStore slabStore) {
		super();

		this.slabStore = slabStore;
	}



	@Override
	public SlabStore requestSlabStore() {
		// TODO Auto-generated method stub
		return slabStore;
	}

}
