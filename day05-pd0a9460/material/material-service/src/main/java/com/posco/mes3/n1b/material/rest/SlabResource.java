package com.posco.mes3.n1b.material.rest;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.posco.mes3.n1b.material.entity.Slab;
import com.posco.mes3.n1b.material.lifecycle.ServiceLifecycle;
import com.posco.mes3.n1b.material.spec.SlabService;
import com.posco.mes3.share.domain.NameValueList;

@RestController
@RequestMapping(value = "/slab")
public class SlabResource implements SlabService{

	private final SlabService slabService;

	
	public SlabResource(ServiceLifecycle serviceLifecycle) {
		this.slabService = serviceLifecycle.requestSlabService();
	}
	
	

	@Override
	@PostMapping(value = {"/", ""})
	public String registerSlab(Slab slab) {
		// TODO Auto-generated method stub
		return slabService.registerSlab(slab);
	}

	@Override
	@GetMapping(value ="/{id}")
	public Slab findSlab(String id) {
		// TODO Auto-generated method stub
		return slabService.findSlab(id);
	}

	@Override
	@PutMapping(value ="/{id}")
	public void modifySlab(@PathVariable(value= "id") String id, NameValueList nameValues) {
		// TODO Auto-generated method stub
		
		slabService.modifySlab(id, nameValues);
	}

	@Override
	@DeleteMapping(value ="/{id}")
	public void removeSlab(String id) {
		// TODO Auto-generated method stub
		slabService.removeSlab(id);
		
	}
	
	
	
	
	
}
