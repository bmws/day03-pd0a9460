package com.posco.mes3.n1b.material.lifecycle;

import org.springframework.stereotype.Component;

import com.posco.mes3.n1b.material.logic.SlabLogic;
import com.posco.mes3.n1b.material.spec.SlabService;
import com.posco.mes3.n1b.material.store.SlabStore;

@Component
public class SerivceLifecycler implements ServiceLifecycle {

	
	private StoreLifecycle storeLifecycle;
	
	
	
	public SerivceLifecycler(StoreLifecycle storeLifecycle) {
		// TODO Auto-generated constructor stub
		this.storeLifecycle = storeLifecycle; 
	}




	@Override
	public SlabService requestSlabService() {
		// TODO Auto-generated method stub
		return new SlabLogic(storeLifecycle);
	}

	
}
