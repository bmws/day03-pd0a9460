package com.posco.mes3.n1b.material;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaterialApp {
    //
    public static void main(String[] args) {
        //
        SpringApplication.run(MaterialApp.class, args);
    }
}
