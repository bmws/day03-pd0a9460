package com.posco.mes3.n1b.material.lifecycle;

import com.posco.mes3.n1b.material.spec.SlabService;

public interface ServiceLifecycle {

	public SlabService requestSlabService();
}
