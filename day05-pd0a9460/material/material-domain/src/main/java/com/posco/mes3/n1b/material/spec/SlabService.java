package com.posco.mes3.n1b.material.spec;

import com.posco.mes3.n1b.material.entity.Slab;
import com.posco.mes3.share.domain.NameValueList;

public interface SlabService {
	//
	public String registerSlab(Slab slab);
	public Slab findSlab(String id);
	public void modifySlab(String id, NameValueList nameValues);
	public void removeSlab(String id);
}
