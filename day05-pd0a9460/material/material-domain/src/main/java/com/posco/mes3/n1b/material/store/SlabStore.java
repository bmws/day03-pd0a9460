package com.posco.mes3.n1b.material.store;

import com.posco.mes3.n1b.material.entity.Slab;

public interface SlabStore {
	//
	public void create(Slab slab);
	public Slab retrieve(String id);
	public void update(Slab slab);
	public void delete(Slab slab);
	
}
