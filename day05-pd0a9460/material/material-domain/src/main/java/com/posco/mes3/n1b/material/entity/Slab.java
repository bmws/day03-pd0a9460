package com.posco.mes3.n1b.material.entity;

import java.util.UUID;

import com.google.gson.Gson;
import com.posco.mes3.share.domain.NameValue;
import com.posco.mes3.share.domain.NameValueList;

public class Slab {
	//
	private String id;
	private Dimension dimension;
	
	public Slab() {
		//
		this.id = UUID.randomUUID().toString();
	}
	
	public Slab(String id) {
		//
		this();
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public static Slab sample() {
		Slab sample = new Slab();
		sample.setDimension(Dimension.sample());
		
		return sample;
		
	}
	
	
	public static void main(String[] args) {
		System.out.println(Slab.sample().toString());
	}
	
	public String toJson() {
		return (new Gson()).toJson(this);
	}
	
	
	public void setId(String id) {
		this.id = id;
	}
	public Dimension getDimension() {
		return dimension;
	}
	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}

	public void setValues(NameValueList nameValues) {
		//
		for (NameValue nameValue: nameValues.getNameValues()) {
			String name = nameValue.getName();
			String value = nameValue.getValue();
			
			switch (name) {
				case "dimension":
					Dimension newDimension = Dimension.fromJson(value);
					if (!newDimension.isValueTickness()) {
						throw new RuntimeException("두께는 300m 미만이어야 합니다.");
					}
					this.dimension = newDimension;
					break;
				default:
					throw new RuntimeException("변경이 허용되지 않는 속성입니다.");
			}
		}
	}
	
}
