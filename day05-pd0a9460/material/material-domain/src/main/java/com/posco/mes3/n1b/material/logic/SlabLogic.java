package com.posco.mes3.n1b.material.logic;

import com.posco.mes3.n1b.material.entity.Slab;
import com.posco.mes3.n1b.material.lifecycle.StoreLifecycle;
import com.posco.mes3.n1b.material.spec.SlabService;
import com.posco.mes3.n1b.material.store.SlabStore;
import com.posco.mes3.share.domain.NameValueList;

public class SlabLogic implements SlabService {
	//
	private final SlabStore slabStore;
	
	public SlabLogic(SlabStore slabStore) {
		//
		this.slabStore = slabStore;
	}
	
	

	public SlabLogic(StoreLifecycle storeLifecycle) {
		// TODO Auto-generated constructor stub
		this.slabStore = storeLifecycle.requestSlabStore();
		
	}



	@Override
	public String registerSlab(Slab slab) {
		//
		this.slabStore.create(slab);
		return slab.getId();
	}

	@Override
	public Slab findSlab(String id) {
		//
		return this.slabStore.retrieve(id);
	}

	@Override
	public void modifySlab(String id, NameValueList nameValues) {
		//
		Slab slab = this.slabStore.retrieve(id);
		slab.setValues(nameValues);
		this.slabStore.update(slab);
	}

	@Override
	public void removeSlab(String id) {
		//
		Slab slab = slabStore.retrieve(id);
		this.slabStore.delete(slab);
	}

}
