package com.posco.mes3.n1b.material.entity;

import java.util.concurrent.ThreadLocalRandom;

import com.google.gson.Gson;

public class Dimension {
    //
    private double width;
    private double height;
    private double thickiness;
    
    public Dimension() {
    	//기본생성자
    }
    
    public boolean isValueTickness() {
    	//
    	
    	return this.thickiness < 300;
    }

    public Dimension(double width, double height, double thickiness) {
        //
        this.width = width;
        this.height = height;
        this.thickiness = thickiness;
    }

    public String toJson() {
        //
        return (new Gson()).toJson(this);
    }

    public static Dimension fromJson(String json) {
        //
        return (new Gson()).fromJson(json, Dimension.class);
    }

    public static Dimension sample() {
        //
        double thickness = ThreadLocalRandom.current().nextDouble(12, 28);
        return new Dimension(10F, 12F, thickness);
    }

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getThickiness() {
		return thickiness;
	}

	public void setThickiness(double thickiness) {
		this.thickiness = thickiness;
	}

}
