package com.posco.mes3.n1b.material.lifecycle;

import com.posco.mes3.n1b.material.store.SlabStore;

public interface StoreLifecycle {

	public SlabStore requestSlabStore() ;
	
}
