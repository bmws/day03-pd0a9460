package com.posco.mes3.share.domain;

import com.google.gson.Gson;

public class NameValue {
    //
    private String name;
    private String value;

    public NameValue(String name, String value) {
        //
        this.setName(name);
        this.setValue(value);
    }

    public static NameValue fromJson(String json) {
        //
        return (new Gson()).fromJson(json, NameValue.class);
    }

    @Override
    public String toString() {
        //
        return (new Gson()).toJson(this);
    }

    public String toSimpleString() {
        //
        return String.format("%s:%s", name, value);
    }

    public static NameValue sample() {
        //
        return new NameValue("name", "Cheolsoo Kim");
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
        System.out.println(sample().toSimpleString());
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
