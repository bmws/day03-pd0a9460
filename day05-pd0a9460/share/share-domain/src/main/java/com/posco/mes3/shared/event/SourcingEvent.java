package com.posco.mes3.shared.event;

import com.google.gson.Gson;
import com.posco.mes3.share.domain.NameValueList;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class SourcingEvent {
    //
    private String id;
    private long time;
    private String chainName;
    private String entityName;
    private EventType eventType;
    private NameValueList nameValues;

    public SourcingEvent() {
        //
        this.id = UUID.randomUUID().toString();
        this.time = System.currentTimeMillis();
    }

    public SourcingEvent(String id, long time) {
        //
        this.id = id;
        this.time = time;
    }

    public SourcingEvent(String chainName, String entityName, EventType eventType, NameValueList nameValues) {
        //
        this.id = UUID.randomUUID().toString();
        this.time = System.currentTimeMillis();
        this.chainName = chainName;
        this.entityName = entityName;
        this.eventType = eventType;
        this.nameValues = nameValues;
    }

    public String getNameValueJson() {
        //
        return (new Gson()).toJson(this.nameValues);
    }

    public static SourcingEvent buildCreateEvent(String chainName, String entityName, NameValueList nameValues) {
        //
        return new SourcingEvent(chainName, entityName, EventType.CREATED, nameValues);
    }

    public static SourcingEvent buildUpdateEvent(String chainName, String entityName, NameValueList nameValues) {
        //
        return new SourcingEvent(chainName, entityName, EventType.UPDATED, nameValues);
    }

    public enum EventType {
        //
        CREATED,
        UPDATED,
        DELETED;
    }
}
