package com.posco.mes3.share.domain;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@Getter
@NoArgsConstructor
public class Dimension {
    //
    private double width;
    private double height;
    private double thickiness;

    public Dimension(double width, double height, double thickiness) {
        //
        this.width = width;
        this.height = height;
        this.thickiness = thickiness;
    }

    public String toJson() {
        //
        return (new Gson()).toJson(this);
    }

    public static Dimension fromJson(String json) {
        //
        return (new Gson()).fromJson(json, Dimension.class);
    }

    public static Dimension sample() {
        //
        double thickness = ThreadLocalRandom.current().nextDouble(12, 28);
        return new Dimension(10F, 12F, thickness);
    }

}
